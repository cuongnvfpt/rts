package rts.rts.service.formService;

import rts.rts.entity.Account;

import java.util.List;

public interface AccountService {
    public List<Account> listAccount();
}
