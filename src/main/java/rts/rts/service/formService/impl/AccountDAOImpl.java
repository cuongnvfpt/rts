package rts.rts.service.formService.impl;

import rts.rts.service.formService.AccountService;
import rts.rts.entity.Account;
import rts.rts.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountDAOImpl implements AccountService {
    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<Account> listAccount() {
        return accountRepository.listAccount();
    }
}
