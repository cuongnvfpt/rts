package rts.rts.controller.homeController;

import rts.rts.service.formService.AccountService;
import rts.rts.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ResponseStatus(HttpStatus.OK)
@CrossOrigin("*")
@RequestMapping(path = "/home")
public class homeController {
    @Autowired
    private AccountService accountDAO;

    @GetMapping("/index")
    public String hello(){
        return "heello";
    }
    @RequestMapping(value = "/account",method = RequestMethod.GET)
    public List<Account> getAccount(){
        return accountDAO.listAccount();
    }
}
